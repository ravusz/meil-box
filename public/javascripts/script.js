// Array contain files .json paths
var jsonFilesArray = [
    ['./json/message-inbox.json'],
    ['./json/messages-send.json'],
    ['./json/messages-important.json'],
    ['./json/messages-drafts.json']
];

/* --- Script functions --- */


// Function getData(data) allows you to read data from .json files
// and change the contents of the message window.
function getData(data) {
    var messageCount = 0;
    var output = "<div class='list-group'>";

    for (var i in data.products) {
        var date = new Date(data.products[i].date);
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var year = date.getFullYear();
        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var name = data.products[i].name;
        var surname = data.products[i].surname;
        var title = data.products[i].title;
        var message = data.products[i].content;
        var meilAddress = data.products[i].email;

        if (attachemnt_elements = data.products[i].attachemnts != 0) {
            var paperclipIconIcon = "<span class='glyphicon glyphicon-paperclip'>";
        } else {
            var paperclipIcon = "";
        } //if

        if (data.products[i].type === 'Clients') {
            var type_of_document = "<span class='badge' style='background-color:#f8ac59;' >Clients</span>";
        } else if (data.products[i].type === 'Documents') {
            var type_of_document = "<span class='badge' style='background-color:#ed5565;'>Documents</span>";
        } else if (data.products[i].type === 'Adv') {
            var type_of_document = "<span class='badge' style='background-color:#23c6c8;'>Adv</span>";
        } else {
            var type_of_document = "<span class='badge'></span>";
        } //if

        if (attachemnt_elements = data.products[i].attachemnts != 0) {
            var paperclipIcon = "<span class='glyphicon glyphicon-paperclip'>";
        } else {
            var paperclipIcon = "";
        } //if

        var isReaded = data.products[i].isReaded;
        var isBold;
        if (isReaded == "true") {
            isBold = "";
            messageCount++;
        } else {
            isBold = "th--notbold";
        } //if

        output += "<a href='#' class='list-group-item ' id='target'>" +

            "<table class='table-responsive  " + isBold + "'>" +
            " <col width='40'>" +
            " <col width='100'>" +
            " <col width='80'>" +
            " <col width='450'>" +
            " <col width='50'>" +
            " <col width='100'>" +
            "<tr >" +
            "<th >" + "<input type='checkbox' name='iCheck'>" + "</th>" +
            "<th class='name '>" + name +
            " " + surname + "</th>" +
            "<th >" + type_of_document + "</th>" +
            "<th class='title'>" + title + "</th>" +
            "<th >" + paperclipIcon + "</th>" +
            "<th >" + monthNames[month] + " " + day + "</th>" +
            "</tr>" +
            "</table>"
    } // for (var i in data.products)

    $('span.badge').text(messageCount);
    output += "</a>";

    document.getElementById('getMeil').innerHTML = output;
    output = "";

    $('input').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    }); //$('input').iCheck

    $("#getMeil a").click(function message() {

        var personIndex = $(this).index();
        console.log(personIndex);

        $.getJSON('./json/message-inbox.json', function(data) {
            title = data.products[personIndex].title;
            message = data.products[personIndex].content;
            meilAddress = data.products[personIndex].email;

            divHeader = "<div class='col-lg-9 div--inbox'>" +
                "<h1>" + "View Message" + "</h1>" +
                "<p>" + "Subject: " + title + "</p>" +
                "<p>" + "From: " + meilAddress + "</p>" +
                "</div>" +
                "<div class='col-lg-3 div--search'>" +

                "<div class='input-group'>" +
                "<button class='btn btn-default btn--float' type='button' >" +
                "<span class='glyphicon glyphicon-trash'></span>" +
                "</button>" +

                "<button class='btn btn-default btn--float' type='button' >" +
                "<span class='glyphicon glyphicon-print'></span>" +
                "</button>" +

                "<button class='btn btn-default btn--float' type='button' >" +
                "<span class='glyphicon glyphicon-share-alt'>Reply</span>" +
                "</button>" +
                "</div>" +
                "<p class='view_message-date'>" + hours + ":" + minutes + " " + day + " " + monthNames[month] + " " + year + "</p>" +

                "</div>";

            var message = data.products[personIndex].content;



            $('#getMeil').html(message).addClass("messages--look");
            $('header').html(divHeader);
            divHeader = "";
        }); //  $.getJSON
    }); //$("#getMeil a").click

    /*name = null;
    surname = null;
    title = null;
    day = null;
    month = null;
    hours = null;
    minutes = null;
    date = null;*/

}; // getData(data)

// Function searchMessage() allows you to get the contents
// of input and select only the messages that
// contain a string of input
function searchMessage() {
    var inputContent = $('.input-group input').val();

    $("#getMeil a").each(function() {
        var contentOfNameAndTitle = $(this).find("th.name, th.title").text();

        if (contentOfNameAndTitle.search(inputContent) >= 0) {
            $(this).show();
        } else {
            $(this).hide();
        } //if
    }); //$("#getMeil a")
}; //function searchMessage()


// Function coloredListDots() allows load colored dot
// before links in " CATEGORIES "
function coloredListDots() {
    var tablica = [
        ['#1ab394'],
        ['#ef5352'],
        ['#1c84c6'],
        ['#23c6c8'],
        ['#f8ac59']
    ];

    for (i = 0; i < 5; i++) {
        $('.circle').eq(i).css('background-color', tablica[i]);
    }; //  for (i = 0; i < 5; i++)
}; // function coloredListDots()



// when the window opens,  follow the steps below
$(document).ready(function() {
    coloredListDots();
    $.getJSON('./json/message-inbox.json', getData);
    headerLayout = $('header').html();


    $("#Inbox").click(function() {
        $.getJSON(jsonFilesArray[0], getData);
        $('header').html(headerLayout);
    });

    $("#Send_Mail").click(function() {
        $.getJSON(jsonFilesArray[1], getData);
        $('header').html(headerLayout);
    });
    $("#Important").click(function() {
        $.getJSON(jsonFilesArray[2], getData);
        $('header').html(headerLayout);
    });
    $("#Drafts").click(function() {
        $.getJSON(jsonFilesArray[3], getData);
        $('header').html(headerLayout);
    });

}); //$(document).ready
