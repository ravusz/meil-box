var Array_links= [['./json/message-inbox.json'],['']];


$(document).ready(function() {


    var tablica = [
        ['#1ab394'],
        ['#ef5352'],
        ['#1c84c6'],
        ['#23c6c8'],
        ['#f8ac59']
    ];
    for (i = 0; i < 5; i++) {
        $('.circle').eq(i).css('background-color', tablica[i]);
    };


    var counter = 0;

    $.getJSON('./json/message-inbox.json', function(data) {
        var output = "<div class='list-group'>";


        for (var i in data.products) {

            var date = new Date(data.products[i].date);

            var day = date.getDate();
            var month = date.getMonth() + 1;
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var year = date.getFullYear();
            var monthNames = [
                "January", "February", "March",
                "April", "May", "June", "July",
                "August", "September", "October",
                "November", "December"
            ];


            var name = data.products[i].name;
            var surname = data.products[i].surname;
            var title = data.products[i].title;
            var message = data.products[i].content;
            var meil_address = data.products[i].email;

            if (attachemnt_elements = data.products[i].attachemnts != 0) {
                var paperclip = "<span class='glyphicon glyphicon-paperclip'>";
            } else {
                var paperclip = "";
            }


            if (data.products[i].type === 'Clients') {
                var type_of_document = "<span class='badge' style='background-color:#f8ac59;' >Clients</span>";
            } else if (data.products[i].type === 'Documents') {
                var type_of_document = "<span class='badge' style='background-color:#ed5565;'>Documents</span>";
            } else if (data.products[i].type === 'Adv') {
                var type_of_document = "<span class='badge' style='background-color:#23c6c8;'>Adv</span>";
            } else {
                var type_of_document = "<span class='badge'></span>";

            }


            if (attachemnt_elements = data.products[i].attachemnts != 0) {
                var paperclip = "<span class='glyphicon glyphicon-paperclip'>";
            } else {
                var paperclip = "";
            }



            var isReaded = data.products[i].isReaded;

            var isBold;
            if (isReaded == "true") {
                isBold = "";
                counter++;
            } else {
                isBold = "th--notbold";
            }

            output += "<a href='#' class='list-group-item ' id='target'>" +

                "<table class='table-responsive  " + isBold + "'>" +
                " <col width='40'>" +
                " <col width='100'>" +
                " <col width='80'>" +
                " <col width='450'>" +
                " <col width='50'>" +
                " <col width='100'>" +
                "<tr >" +
                "<th >" + "<input type='checkbox' name='iCheck'>" + "</th>" +
                "<th class='name '>" + name +
                " " + surname + "</th>" +
                "<th >" + type_of_document + "</th>" +
                "<th class='title'>" + title + "</th>" +
                "<th >" + paperclip + "</th>" +
                "<th >" + monthNames[month] + " " + day + "</th>" +

                "</tr>" +
                "</table>"
        }

        $('span.badge').text(counter);
        output += "</a>";



        document.getElementById('getMeil').innerHTML = output;
        output = "";

        $('input').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
            increaseArea: '20%' // optional
        });

        $("#getMeil a").click(function message() {

            var index_person = $(this).index();
            console.log(index_person);

            $.getJSON('./json/message-inbox.json', function(data) {

                title = data.products[index_person].title;
                message = data.products[index_person].content;
                meil_address = data.products[index_person].email;

                div_header = "<div class='col-lg-9 div--inbox'>" +
                    "<h1>" + "View Message" + "</h1>" +
                    "<p>" + "Subject: " + title + "</p>" +
                    "<p>" + "From: " + meil_address + "</p>" +
                    "</div>" +
                    "<div class='col-lg-3 div--search'>" +

                    "<div class='input-group'>" +
                    "<button class='btn btn-default btn--float' type='button' >" +
                    "<span class='glyphicon glyphicon-trash'></span>" +
                    "</button>" +

                    "<button class='btn btn-default btn--float' type='button' >" +
                    "<span class='glyphicon glyphicon-print'></span>" +
                    "</button>" +

                    "<button class='btn btn-default btn--float' type='button' >" +
                    "<span class='glyphicon glyphicon-share-alt'>Reply</span>" +
                    "</button>" +
                    "</div>" +
                    "<p class='view_message-date'>" + hours + ":" + minutes + " " + day + " " + monthNames[month] + " " + year + "</p>" +



                    "</div>";

                var message = data.products[index_person].content;

                $('#getMeil').html(message).addClass("messages--look");
                $('header').html(div_header);
            });

        });
        // name = null;
        // surname = null;
        // title = null;
        // day = null;
        // month = null;
        // hours = null;
        // minutes = null;
        // date = null;

    });
});


//Function search
function search() {
    var input_content = $('.input-group input').val();

    $("#getMeil a").each(function() {
        var sprawdz = $(this).find("th.name, th.title").text();

        if (sprawdz.search(input_content) >= 0) {

            $(this).show();
        } else {
            $(this).hide();
        }
    });


};
